This is a resource from a project developed by eXO.cat to help the startup of a micro ISP that provides ADSLs to clients in the framework of professional community networks and commons governance through guifi.net

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Introduction](#introduction)
- [How to provision?](#how-to-provision)
  - [Plaintext file way](#plaintext-file-way)
    - [CSV file: Generate user and password](#csv-file-generate-user-and-password)
  - [API/Database way](#apidatabase-way)
- [License (unless specified)](#license-unless-specified)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Introduction

The micro ISP orders routers with a particular generic configuration to the manufacturer. [GenieACS](https://genieacs.com/) applies a specific configuration for a specific user (through its unique serial number). The most important thing is to apply a [PPP](https://en.wikipedia.org/wiki/Point-to-Point_Protocol) user that belongs to a specific [network profile](https://blog.khophi.co/daloradius-profiles/), for example: NAT, dynamic IP, static IP. This can be done with a [PPP server](https://ppp.samba.org/) (untested, `apt-get install ppp`) and [daloradius](http://www.daloradius.com/).

# Requirements

Assuming Debian 9

`apt-get install python3-setuptools python3-pip`

`pip3 install ansible yaml`

# How to provision?

There are two ways to do the provision. Through an API/Database or with a plaintext file. Choose

## Plaintext file way

- Pros: You don't need to run another service, everything in that file
- Con: Everything in that file means more complexity on it. Be organized!

**Execute** `genconf.sh` to generate the provisions (you have an example/demo to try it)

How it works? (Workflow)

For each device model:

- Given a CSV file ([nc124](https://github.com/guifi-exo/start-microisp/blob/master/users_nc124.csv), [nc224](https://github.com/guifi-exo/start-microisp/blob/master/users_nc224.csv))
- Generate a YAML file with [csv2yaml.py](https://github.com/guifi-exo/start-microisp/blob/master/csv2yaml.py)
- Ansible processes the input files of the YAML file ([nc124](https://github.com/guifi-exo/start-microisp/blob/master/users_nc124.yml), [nc224](https://github.com/guifi-exo/start-microisp/blob/master/users_nc224.yml)) and the [JINJA template file](https://github.com/guifi-exo/start-microisp/blob/master/provision.js.j2) to generate the provisions for GenieACS ([nc124](https://github.com/guifi-exo/start-microisp/blob/master/provision_nc124.js), [nc224](https://github.com/guifi-exo/start-microisp/blob/master/provision_nc224.js))

### CSV file tool

Libreoffice calc is suggested to:

- generate CSV file. From there I saved as csv file.
- generate user putting in a user column the identifier and extended it: nc124_1 to nc124_n and nc224_1 to nc224_n.
- generate password pasting results of `genpass.sh` (look at the length and number of passwords variables) in a password column.
- ask manufacturer for an CSV containing serial numbers of routers, or collect them manually

### GenieACS

[GenieACS](https://genieacs.com/) automates the configuration.

Configure a preset that triggers the provision (javascript file)

Two models, two presets

![list presets](img/listing_presets.png)

Preset 124

![list presets](img/preset_124.png)

Preset 224

![list presets](img/listing_provision.png)

The list of provisions

An example of provision for 124, execute `genconf.sh` to see the results in detail

![list presets](img/provision_example.png)

## API/Database way

- Pro: nice integration with the rest of services of your enterprise: if you have ERP, CRM, database. Provision script is short and simple.
- Con: It's another service running, you have to develop and maintain it.

A message from [Dan in GenieACS user's mailinglist](http://lists.genieacs.com/pipermail/users/2017-June/001535.html) suggests this API/Database provision way.

How it works? Provision script calls an external script that does an API client request to the API server to get user, password and/or other specific values.

Provision script example:

```
// WTFPL license http://www.wtfpl.net/

const now = Date.now();
let serialNumber = declare("DeviceID.SerialNumber", {value: 1}).value[0];
let productClass = declare("DeviceID.ProductClass", {value: 1}).value[0];
let oui = declare("DeviceID.OUI", {value: 1}).value[0];
let args = {serial: serialNumber, productClass: productClass, oui: oui};

//Get the PPPoE creds
let config = ext('cpe-config', 'resetPppoe', JSON.stringify(args));
if (!config) {
  log('No config returned from API');
  return;
}

// ensures that there is 1 WANPPPConnection instance
declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*", {value: now}, {path: 1});

// set PPPoE user and password
declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Username", {value: now}, {value: config.username});
declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Password", {value: now}, {value: config.password});
```

External script example:

```
// WTFPL license http://www.wtfpl.net/

//"use strict";
 
const API_URL = process.env.API_URL || 'https://YOUR_URL/api/ACS/';
const url = require("url");
const http = require(API_URL.split(":", 1)[0]);
const extend = require('util')._extend
 
function resetPppoe(args, callback) {
  let params = JSON.parse(args[0]);
 
  const uri = API_URL + "ResetPPPoECreds?serial=" + params.serial + '&productClass=' + params.productClass + '&oui=' + params.oui;
 
  console.log({ uri: uri, serial: params.serial });
 
  let options = url.parse(uri);
  options.headers = {
    accept: 'application/json',
    "content-type": 'application/json'
  };
 
  let request = http.get(options, function (response) {
    if (response.statusCode == 404) {
      return callback(null, null);
    }
 
    if (response.statusCode >= 400) {
      return callback(new Error("Unexpected error resetting PPPoE credentials. Response Code: " + response.statusCode + '. Status Message: ' + response.statusMessage + '. t: ' + typeof response.statusCode));
    }
 
    let data = "";
    response.on("data", function (d) {
      data = data + d.toString();
    });
 
    response.on("end", function () {
      let result = JSON.parse(data);
 
      console.log('Returning credentials to client', {username: result.username, password: result.password, settings: result.settings, serial: params.serial });
      return callback(null, result);
    });
  });
 
  request.on("error", function (err) {
    console.log('args');
    console.log(arguments);
    callback(err);
  });
}
 
function getConfig(args, callback) {
  let params = JSON.parse(args[0]);
 
  const uri = API_URL + "Config?serial=" + params.serial + '&productClass=' + params.productClass + '&oui=' + params.oui;
  console.log({ message: uri, serial: params.serial });
 
  let options = url.parse(uri);
  options.headers = {
    accept: 'application/json',
    "content-type": 'application/json'
  };
 
  let request = http.get(options, function (response) {
    if (response.statusCode >= 400) {
      return callback(new Error("Unexpected error getting CPE config mode. Response Code: " + response.statusCode + '. Status Message: ' + response.statusMessage));
    }
 
    let data = "";
    response.on("data", function (d) {
      data = data + d.toString();
    });
 
    response.on("end", function () {
      let result = JSON.parse(data);
 
      console.log('Config mode: ' + result.config, {serial: params.serial });
      return callback(null, result);
    });
  });
 
  request.on("error", function (err) {
    console.log('args');
    console.log(arguments);
    callback(err);
  });
}
 
exports.resetPppoe = resetPppoe;
exports.getConfig = getConfig;
```

# License (unless specified)

- Software; code and scripts: https://www.gnu.org/licenses/gpl-3.0.en.html
- Documentation: https://creativecommons.org/licenses/by-sa/4.0/
