# thanks! https://serverfault.com/questions/261086/creating-random-password

# why trust urandom? https://media.ccc.de/v/32c3-7441-the_plain_simple_reality_of_entropy

ncharacters=10
npasswords=1000

< /dev/urandom tr -dc 'a-zA-Z0-9' | fold -w $ncharacters | head -n $npasswords
