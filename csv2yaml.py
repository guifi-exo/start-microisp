#!/usr/bin/env python3
# portability -> src https://stackoverflow.com/questions/7670303/purpose-of-usr-bin-python3

# Copyright (C) 2017 guifipedro
# This is free software, licensed under the GNU General Public License v3.

import os, sys, csv
import yaml
from yaml import dump
import argparse

# check python3 -> src https://stackoverflow.com/questions/1093322/how-do-i-check-what-version-of-python-is-running-my-script
if sys.version_info < (3,0):
    raise Exception("Python 3 or a more recent version is required.")

parser = argparse.ArgumentParser()
# https://stackoverflow.com/questions/28638813/how-to-make-a-short-and-long-version-of-a-required-argument-using-python-argpars
parser.add_argument("-i", "--input-file", type=str, help="input csv file with information about user, password and sn of model")
parser.add_argument("-o", "--output-file", type=str, help="output csv file for import users on daloradius")
parser.add_argument("-m", "--model", type=str, help="device model, at the moment (2017-06-14): nc124 or nc224")
parser.add_argument("-d", "--domain", type=str, help="FQDN or domain of your user. more info: https://www.broadband-forum.org/technical/download/TR-025.pdf")
parser.add_argument("-s", "--proxy-server", type=str, help="define proxy server in case of model nc224")
parser.add_argument("-p", "--proxy-server-port", type=str, help="define proxy server port in case of model nc224")
args = parser.parse_args()

# parsed variables
# Dashes are converted to underscores -> src https://stackoverflow.com/a/12834980
i_csvpath = args.input_file
o_csvpath = args.output_file
model = args.model
domain = args.domain
proxy_server = args.proxy_server
proxy_server_port = args.proxy_server_port

# users object
users = []

# write file -> src https://www.digitalocean.com/community/tutorials/how-to-handle-plain-text-files-in-python-3
# append -> src https://stackoverflow.com/questions/4706499/how-do-you-append-to-a-file
out = open(o_csvpath,'a')
# output file headers
out.write('pppoe_user' + ',' + 'pppoe_passwd' + '\n')

with open(i_csvpath) as f:
    # read csv with headers -> src https://stackoverflow.com/questions/3428532/how-to-import-a-csv-file-using-python-with-headers-intact-where-first-column-is
    # comment: if you put spaces between separators you will have a key error! ;)
    reader = csv.DictReader(f)
    for line in reader:
        # DEBUG CSV: be careful, debug goes to yaml file
        #print("user", line['user'], "pass", line['passwd'])

        # variables to reuse
        pppoe_user = line['user'] + '@' + domain
        pppoe_passwd = line['passwd']

        # ref: http://stackoverflow.com/questions/12470665/how-can-i-write-data-in-yaml-format-in-a-file/12471272#12471272
        users.append({
            'id': line['user'],
            'pppoe_user': pppoe_user,
            'pppoe_passwd': pppoe_passwd,
            'sn': line['sn'],
            'mac': line['mac'],
        })
        # FIXME at the moment entered by hand because of other reaons, but how to insert this conditionally to the specific previous object?
        #if model == 'nc224':
        #    users.append({
        #        'sip_user': line['sip_user'],
        #        'sip_passwd': line['sip_passwd'],
        #    })

        # do an output file for daloradius import of pppoe user and password
        out.write(pppoe_user + ',' + pppoe_passwd + '\n')

    root = {
        'model': model,
        'proxy_server': proxy_server,
        'proxy_server_port': proxy_server_port,
        'users': users
    }
    print(yaml.dump(root, default_flow_style=False))
