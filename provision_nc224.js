// file for model nc224

// get current date
const now = Date.now();

// get serial number
let serial = declare("InternetGatewayDevice.DeviceInfo.SerialNumber", { value: 1 }).value[0];

if ( serial === "1" ) {

    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Username", {value: now}, {
      value: "nc224_1@myisp@xdsl"
    });
    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Password", {value: now}, {
      value: "McpBvff64s"
    });declare("InternetGatewayDevice.X_CT-COM_UserInfo.UserList.1.Password", { value: now }, {
      value: "McpBvff64s"
    });
    // patching/fixing a bad value at master configuration
    declare("InternetGatewayDevice.X_CT-COM_Firewall.CurrentLANFilterTable", { value: now }, {
      value: "0"
    });

    // SIP proxy server
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.RegistrarServer", { value: now}, {
      value: "my-proxy-sip.example.com"
    });
    // SIP proxy server port
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.OutboundProxyPort", { value: now}, {
      value: "5060"
    });
    // Enable SIP line
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.Enable", { value: now }, {
      value: "Enabled"
    });

    // END provision
    declare("Tags.configured", { value: now }, { value: true });
    declare("Tags.order_20170710", { value: now}, { value: true });
} else if ( serial === "2" ) {

    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Username", {value: now}, {
      value: "nc224_2@myisp@xdsl"
    });
    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Password", {value: now}, {
      value: "IkRmyPdMjv"
    });declare("InternetGatewayDevice.X_CT-COM_UserInfo.UserList.1.Password", { value: now }, {
      value: "IkRmyPdMjv"
    });
    // patching/fixing a bad value at master configuration
    declare("InternetGatewayDevice.X_CT-COM_Firewall.CurrentLANFilterTable", { value: now }, {
      value: "0"
    });

    // SIP proxy server
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.RegistrarServer", { value: now}, {
      value: "my-proxy-sip.example.com"
    });
    // SIP proxy server port
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.OutboundProxyPort", { value: now}, {
      value: "5060"
    });
    // Enable SIP line
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.Enable", { value: now }, {
      value: "Enabled"
    });

    // END provision
    declare("Tags.configured", { value: now }, { value: true });
    declare("Tags.order_20170710", { value: now}, { value: true });
} else if ( serial === "3" ) {

    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Username", {value: now}, {
      value: "nc224_3@myisp@xdsl"
    });
    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Password", {value: now}, {
      value: "PSWXEJO8WS"
    });declare("InternetGatewayDevice.X_CT-COM_UserInfo.UserList.1.Password", { value: now }, {
      value: "PSWXEJO8WS"
    });
    // patching/fixing a bad value at master configuration
    declare("InternetGatewayDevice.X_CT-COM_Firewall.CurrentLANFilterTable", { value: now }, {
      value: "0"
    });

    // SIP proxy server
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.RegistrarServer", { value: now}, {
      value: "my-proxy-sip.example.com"
    });
    // SIP proxy server port
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.OutboundProxyPort", { value: now}, {
      value: "5060"
    });
    // Enable SIP line
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.Enable", { value: now }, {
      value: "Enabled"
    });

    // END provision
    declare("Tags.configured", { value: now }, { value: true });
    declare("Tags.order_20170710", { value: now}, { value: true });
} else if ( serial === "4" ) {

    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Username", {value: now}, {
      value: "nc224_4@myisp@xdsl"
    });
    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Password", {value: now}, {
      value: "CTsi8R6BZC"
    });declare("InternetGatewayDevice.X_CT-COM_UserInfo.UserList.1.Password", { value: now }, {
      value: "CTsi8R6BZC"
    });
    // patching/fixing a bad value at master configuration
    declare("InternetGatewayDevice.X_CT-COM_Firewall.CurrentLANFilterTable", { value: now }, {
      value: "0"
    });

    // SIP proxy server
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.RegistrarServer", { value: now}, {
      value: "my-proxy-sip.example.com"
    });
    // SIP proxy server port
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.OutboundProxyPort", { value: now}, {
      value: "5060"
    });
    // Enable SIP line
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.Enable", { value: now }, {
      value: "Enabled"
    });

    // END provision
    declare("Tags.configured", { value: now }, { value: true });
    declare("Tags.order_20170710", { value: now}, { value: true });
} else if ( serial === "5" ) {

    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Username", {value: now}, {
      value: "nc224_5@myisp@xdsl"
    });
    declare("InternetGatewayDevice.WANDevice.*.WANConnectionDevice.*.WANPPPConnection.*.Password", {value: now}, {
      value: "mcVXYbOCGh"
    });declare("InternetGatewayDevice.X_CT-COM_UserInfo.UserList.1.Password", { value: now }, {
      value: "mcVXYbOCGh"
    });
    // patching/fixing a bad value at master configuration
    declare("InternetGatewayDevice.X_CT-COM_Firewall.CurrentLANFilterTable", { value: now }, {
      value: "0"
    });

    // SIP proxy server
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.RegistrarServer", { value: now}, {
      value: "my-proxy-sip.example.com"
    });
    // SIP proxy server port
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.SIP.OutboundProxyPort", { value: now}, {
      value: "5060"
    });
    // Enable SIP line
    declare("InternetGatewayDevice.Services.VoiceService.1.VoiceProfile.1.Line.1.Enable", { value: now }, {
      value: "Enabled"
    });

    // END provision
    declare("Tags.configured", { value: now }, { value: true });
    declare("Tags.order_20170710", { value: now}, { value: true });
}
