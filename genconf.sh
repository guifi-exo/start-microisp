#!/bin/bash

# Copyright (C) 2017 guifipedro
# This is free software, licensed under the GNU General Public License v3.

# Generate config files
# Adapt this script to your specific circunstances

# Variables
input_file_nc124="./users_nc124.csv"
input_file_nc224="./users_nc224.csv"
ppp_fqdn="myisp@xdsl"
proxy_server="my-proxy-sip.example.com"
proxy_server_port="5060"
output_file="./import_to_daloradius.csv"

# shortcut to avoid rm dialog
if [ "$1" == "y" ]; then
    rm -f "$output_file" # because after that output file is appended
else
    rm -i -f "$output_file" # because after that output file is appended
fi

# nc124 model
python3 csv2yaml.py -m nc124 -i "$input_file_nc124" -o "$output_file" -d "$ppp_fqdn" > users_nc124.yml
ansible-playbook -c local -i localhost, playbook_local_nc124.yml # out: provision_nc124.js

## nc224 model
python3 csv2yaml.py -m nc224 -i "$input_file_nc224" -o "$output_file" -d "$ppp_fqdn" -s "$proxy_server" -p "$proxy_server_port" > users_nc224.yml
ansible-playbook -c local -i localhost, playbook_local_nc224.yml # out: provision_nc224.js
